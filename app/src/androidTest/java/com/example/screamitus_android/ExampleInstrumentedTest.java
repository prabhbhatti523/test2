package com.example.screamitus_android;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import android.content.Context;
import android.support.test.InstrumentationRegistry;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

import static org.junit.Assert.assertEquals;
/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.screamitus_android", appContext.getPackageName());
    }

    // Put the name of the screen you want android to start on
    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);
    // TC1: when app loads, text box and button is visible but result lable is not  visible
    @Test
    public void appLoads() throws InterruptedException {



        onView(withId(R.id.daysTextBox)).check(matches(isDisplayed()));
        onView(withId(R.id.resultsLabel)).check(matches((isDisplayed())));
        onView(withText("Calculate")).check(matches(isDisplayed()));


    }
    //TC2 text box shows correct value in the text box
    @Test
    public void TC2() throws InterruptedException {
        Infection infection = new Infection();


        onView(withId(R.id.daysTextBox))
                .perform(typeText("8"));

        String expectedOutput = "43 instructors infected";
        onView(withText("Calculate")).perform(click());
        onView(withId(R.id.resultsLabel))
                .check(matches(withText(expectedOutput)));
    }


}
