package com.example.screamitus_android;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    Infection infection;

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Before
    public void setUp(){
        infection = new Infection();
    }

    //R1: number of days must be bigger than 0
    @Test
    public void daysMoreThanZero(){

        int numInfected = infection.calculateTotalInfected(0);
        assertEquals(-1, numInfected);

    }
    //R2: the virus infects the instructor at 5 instructor per day
    @Test
    public void infectFiveInstructorPerDay(){

        int numInfected = infection.calculateTotalInfected(2);
        assertEquals(10, numInfected);

    }
//R3: after 7 days rate go to 8 intructor per day
@Test
public void infectEightInstructorPerDayAfterSevenDays(){

    int numInfected = infection.calculateTotalInfected(10);
    assertEquals(59, numInfected);

}

//R4: pritesh, mohammed and albert are at college at even days

    @Test
    public void infectAtEvenDays(){

        int numInfected = infection.calculateTotalInfected(10);
        assertEquals(59, numInfected);

    }
}